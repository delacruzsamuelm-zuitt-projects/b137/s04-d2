package b137.delacruz.s04d2.abstraction;

public class Person implements Actions, SpecialSkills {

    // Constructor

    public Person() {}

    // Methods from Action Interface

    public void sleep() {
       Actions.super.sleep();
    }

    public void run() {
       Actions.super.run();
    }

    // Methods from SpecialSkill Interface
    public void computerProgram() {
        SpecialSkills.super.computerProgram();
    }

    public void driveACar() {
        SpecialSkills.super.driveACar();
    }


}
