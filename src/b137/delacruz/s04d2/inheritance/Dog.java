package b137.delacruz.s04d2.inheritance;

public class Dog extends Animal {
    // Properties
    private String breed;

    // Constructor - need to put super() function
    public Dog() {
        super();
    }
    public Dog(String name, String color, String breed) {
        super(name, color);
        this.breed = breed;
    }

    // Getters & Setters
    public String getBreed() {

        return breed;
    }

    public void setBreed(String newBreed) {
        this.breed = newBreed;
    }

    // Methods
    public void bark() {

        super.showDetails();
    }

    // Overwriting the showDetails() in parent Animal Class. - Polymorphism
    public void showDetails() {
        System.out.println("I am " + super.getName() + ", with color " + super.getColor() + " and breed of " + this.breed);
    }
}
